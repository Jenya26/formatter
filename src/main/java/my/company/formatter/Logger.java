package my.company.formatter;

public interface Logger {

    void debug( String string, Object... object);
    void info( String string, Object... object);
    void error( String string, Object... object);
    void warn(String string, Object... object);

}
