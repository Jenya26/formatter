package my.company.formatter;

import my.company.formatter.configuration.XlsConfiguration;
import my.company.formatter.context.MapContext;
import my.company.formatter.formatter.Formatter;
import my.company.formatter.formatter.FormatterImpl;
import my.company.formatter.streams.*;
import my.company.formatter.tokenizer.TreeTokenizer;

import java.io.FileNotFoundException;


public class Main {

    public static void main( String[] argv) throws FileNotFoundException {
        MapContext context = new MapContext();
        XlsConfiguration handlers = new XlsConfiguration();
        XlsConfiguration states = new XlsConfiguration();
        TreeTokenizer tokenizer = new TreeTokenizer();
        Formatter formatter = new FormatterImpl();
        InputStream inputStream = new FileInputStream( "input.txt");
        OutputStream outputStream = new FileOutputStream( "output.txt");

        handlers.load( new ResourceInputStream("formatter.xlsx"), "handlers");
        states.load( new ResourceInputStream("formatter.xlsx"), "states");
        tokenizer.load(new ResourceInputStream( "tokens.list"));
        context.load( new ResourceInputStream( "context.xml"));
        formatter.execute( context, handlers, states, inputStream, outputStream, tokenizer);
    }
}
