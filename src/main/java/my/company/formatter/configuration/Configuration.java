package my.company.formatter.configuration;

public interface Configuration {

    String getParameter( String key1, String key2);

}
