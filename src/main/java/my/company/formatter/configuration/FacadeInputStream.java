package my.company.formatter.configuration;

import java.io.IOException;
import java.io.InputStream;

public class FacadeInputStream extends InputStream {

    private my.company.formatter.streams.InputStream inputStream;

    public FacadeInputStream(my.company.formatter.streams.InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public int read() throws IOException {
        int compare = Boolean.compare( inputStream.hasNext(), false);
        return inputStream.next() * compare - (1 - compare);
    }

}
