package my.company.formatter.configuration;

import my.company.formatter.streams.InputStream;

import java.util.*;
import java.util.function.Function;

public class MapConfiguration implements Configuration {

    private Map<String, Map<String, String>> configuration;

    public MapConfiguration() {
        this.configuration = new HashMap<>();
    }

    public void load( InputStream inputStream){
        StringBuilder stringBuilder = new StringBuilder();
        String end = getValues( stringBuilder, inputStream).get( 0);
        List<String> states = getValues( stringBuilder, inputStream);
        readLine( stringBuilder, inputStream);
        List<String> values;
        Iterator<String> iterator;
        String token;
        String value;
        int state;
        values = getValues( stringBuilder, inputStream);
        do {
            iterator = values.iterator();
            token = iterator.next().trim();
            token = token.substring( 1, token.length() - 1).replace( "\\n", "\n").replace( "\\r", "\r");
            state = 1;
            while( iterator.hasNext()) {
                value = iterator.next().trim();
                value = value.substring( 1, value.length() - 1);
                this.configuration.computeIfAbsent(token, new Function<String, Map<String, String>>() {
                    @Override
                    public Map<String, String> apply(String s) {
                        return new HashMap<>();
                    }
                }).put( states.get( state), value);
                ++state;
            }
            values = getValues( stringBuilder, inputStream);
        } while ( !values.get( 0).equals( end) );
    }

    private List<String> getValues(StringBuilder stringBuilder, InputStream inputStream) {
        List<String> list = new LinkedList<>();
        char separator = inputStream.next();
        char character;
        while( inputStream.hasNext() && (character = inputStream.next()) != '\n') {
            stringBuilder.delete( 0, stringBuilder.length());
            stringBuilder.append( character);
            while( inputStream.hasNext() && (character = inputStream.next()) != separator && character != '\r') {
                stringBuilder.append( character);
            }
            inputStream.next();
            list.add( stringBuilder.toString().trim());
        }
        return list;
    }

    private void readLine( StringBuilder stringBuilder, InputStream inputStream) {
        stringBuilder.delete( 0, stringBuilder.length());
        char character;
        while( inputStream.hasNext() && (character = inputStream.next()) != '\n'){
            stringBuilder.append( character);
        }
    }

    @Override
    public String getParameter(String key1, String key2) {
        return configuration.computeIfAbsent(key1, new Function<String, Map<String, String>>() {
            @Override
            public Map<String, String> apply(String s) {
                return configuration.get( "default");
            }
        }).get( key2);
    }

}
