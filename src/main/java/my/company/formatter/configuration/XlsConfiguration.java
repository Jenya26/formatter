package my.company.formatter.configuration;

import my.company.formatter.streams.InputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;

public class XlsConfiguration implements Configuration {

    private Map<String, Map<String, String>> configuration;

    public XlsConfiguration() {
        this.configuration = new HashMap<>();
    }

    public void load( InputStream inputStream, String sheet){
        XSSFWorkbook myExcelBook = null;
        try {
            myExcelBook = new XSSFWorkbook(new FacadeInputStream(inputStream));
            XSSFSheet myExcelSheet = myExcelBook.getSheet(sheet);
            Iterator<Row> rowIterator = myExcelSheet.iterator();
            rowIterator.next();
            while( rowIterator.hasNext()){
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                Iterator<Cell> statesIterator = myExcelSheet.getRow(0).cellIterator();
                statesIterator.next();
                Map<String, String> token = configuration.computeIfAbsent(
                        cellIterator.next().getStringCellValue().replace( "\\r", "\r").replace( "\\n", "\n"),
                        new Function<String, Map<String, String>>() {
                            @Override
                            public Map<String, String> apply(String s) {
                        return new HashMap<String, String>();
                    }
                        });
                while( cellIterator.hasNext() && statesIterator.hasNext()) {
                    token.put( statesIterator.next().getStringCellValue(), cellIterator.next().getStringCellValue());
                }
            }
            myExcelBook.close();
        } catch (IOException e) {
            throw new my.company.formatter.streams.IOException( e);
        }
    }

    @Override
    public String getParameter(String key1, String key2) {
        return configuration.computeIfAbsent(key1, new Function<String, Map<String, String>>() {
            @Override
            public Map<String, String> apply(String s) {
                return configuration.get( "default");
            }
        }).get( key2);
    }

}
