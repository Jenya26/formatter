package my.company.formatter.context;

public interface Context {

    <T> T getInstance( String name);

}
