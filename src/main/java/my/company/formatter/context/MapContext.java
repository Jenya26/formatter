package my.company.formatter.context;

import my.company.formatter.Logger;
import my.company.formatter.logger.LoggerFactory;
import my.company.formatter.streams.InputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class MapContext implements Context {

    private Logger logger = LoggerFactory.getLogger( MapContext.class);

    private Map<String, Object> container;

    public MapContext() {
        this.container = new HashMap<>();
    }

    public MapContext( MapContext context) {
        this.container = new HashMap<>(context.container);
    }

    @Override
    public <T> T getInstance(String name) {
        return (T) container.get(name);
    }

    public void load(InputStream inputStream){
        FacadeInputStream facadeInputStream = new FacadeInputStream( inputStream);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(facadeInputStream);
            loadContext( "Context", this, (Element) doc.getDocumentElement());
        } catch (ParserConfigurationException e) {
            throw new ContextException( e);
        } catch (SAXException e) {
            throw new ContextException( e);
        } catch (IOException e) {
            throw new ContextException( e);
        }

    }

    private void loadContext( String rootName, MapContext context, Element doc){
        NodeList binds = doc.getElementsByTagName( "bind");
        int length = binds.getLength();
        String name;
        for( int i=0; i<length; ++i) {
            if( binds.item( i).getParentNode() != doc) continue;
            Element bind = (Element) binds.item( i);
            MapContext temp = new MapContext( this);
            name = String.format( "%s > %s", rootName, bind.getElementsByTagName( "name").item( 0).getTextContent());
            loadContext( name, temp, (Element) bind.getElementsByTagName( "context").item( 0));
            try {
                context.container.put( bind.getElementsByTagName( "name").item( 0).getTextContent(), getInstance(bind, temp));
                logger.info("Instance ({}) has been loaded", name);
            }catch (ContextException ex){
                logger.error("Instance ({}) was not loaded, ex ", name, ex);
                throw ex;
            }
        }
    }

    private Object getInstance( Element element, MapContext context){
        try {
            NodeList arguments = ((Element)element.getElementsByTagName( "constructor").item( 0)).getElementsByTagName( "argument");
            Object[] constructorArguments = new Object[arguments.getLength()];
            for( int i=0; i<arguments.getLength(); ++i) {
                Element argument = ((Element) arguments.item( i));
                String name = argument.getElementsByTagName( "name").item( 0).getTextContent();
                String value = argument.getElementsByTagName( "value").item( 0).getTextContent();
                constructorArguments[i] = context.container.getOrDefault( name, value);
            }
            Constructor[] constructors = Class.forName( ((Element)element.getElementsByTagName( "path").item( 0)).getTextContent()).getConstructors();
            for( Constructor constructor: constructors){
                Class[] parameters = constructor.getParameterTypes();
                boolean check = parameters.length == constructorArguments.length;
                for( int i=0; i<parameters.length && check; ++i){
                    try {
                        check &= (parameters[i].isAssignableFrom(constructorArguments[i].getClass()) || constructorArguments[i].getClass().getField("TYPE").get(null).equals(parameters[i]));
                    }catch(NoSuchFieldException e){
                        check = false;
                    }
                }
                if( check) return constructor.newInstance( constructorArguments);
            }
        } catch (ClassNotFoundException e) {
            throw new ContextException( e);
        } catch (IllegalAccessException e) {
            throw new ContextException( e);
        } catch (InstantiationException e) {
            throw new ContextException( e);
        } catch (InvocationTargetException e) {
            throw new ContextException( e);
        }
        logger.error( "Constructor '{}' not found", element.getElementsByTagName( "name").item( 0).getTextContent());
        throw new ContextException( "Constructor not found");
    }

}
