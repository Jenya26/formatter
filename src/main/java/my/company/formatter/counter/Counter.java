package my.company.formatter.counter;

public interface Counter {

    long getCount();
    void setCount( long count);

}
