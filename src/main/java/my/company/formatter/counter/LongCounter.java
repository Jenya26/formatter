package my.company.formatter.counter;

public class LongCounter implements Counter {

    private long counter;

    public LongCounter(long counter) {
        this.counter = counter;
    }

    @Override
    public long getCount() {
        return counter;
    }

    @Override
    public void setCount( long counter) {
        this.counter = counter;
    }
}
