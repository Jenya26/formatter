package my.company.formatter.exceptions;

public class HandlerException extends RuntimeException {

    public HandlerException(){
        super();
    }

    public HandlerException(String message){
        super( message);
    }

    public HandlerException(String message, Throwable cause){
        super( message, cause);
    }

    public HandlerException(Throwable cause){
        super( cause);
    }
}
