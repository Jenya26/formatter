package my.company.formatter.exceptions;

public class ObjectFactoryException extends RuntimeException {

    public ObjectFactoryException(){
        super();
    }

    public ObjectFactoryException(String message){
        super( message);
    }

    public ObjectFactoryException(String message, Throwable cause){
        super( message, cause);
    }

    public ObjectFactoryException(Throwable cause){
        super( cause);
    }
}
