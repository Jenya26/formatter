package my.company.formatter.formatter;

import my.company.formatter.configuration.Configuration;
import my.company.formatter.context.Context;
import my.company.formatter.streams.InputStream;
import my.company.formatter.streams.OutputStream;
import my.company.formatter.tokenizer.Tokenizer;

public interface Formatter {

    void execute(Context context, Configuration handlers, Configuration states, InputStream inputStream, OutputStream outputStream, Tokenizer tokenizer);

}
