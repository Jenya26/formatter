package my.company.formatter.formatter;

import my.company.formatter.Logger;
import my.company.formatter.configuration.Configuration;
import my.company.formatter.context.Context;
import my.company.formatter.logger.LoggerFactory;
import my.company.formatter.streams.InputStream;
import my.company.formatter.streams.OutputStream;
import my.company.formatter.tokenizer.Tokenizer;

import java.util.Stack;

public class FormatterImpl implements Formatter {

    private static final Logger logger = LoggerFactory.getLogger( FormatterImpl.class);

    public void execute(Context context, Configuration handlers, Configuration states, InputStream inputStream, OutputStream outputStream, Tokenizer tokenizer) {
        String token;
        String state = "general";
        String handlerName;
        while( inputStream.hasNext()) {
            token = tokenizer.nextToken(inputStream);
            handlerName = handlers.getParameter(token, state);
            try {
                Handler handler = context.<Handler>getInstance(handlerName);
                outputStream.write(handler.execute(token));
            } catch (NullPointerException ex) {
                String message = String.format( "Handler '%s' not found", handlerName);
                logger.error( message, ex);
                throw new FormatterException( message, ex);
            }
            state = states.getParameter(token, state);
        }
    }
}
