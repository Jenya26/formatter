package my.company.formatter.formatter;

public interface Handler {

    String execute( String input);

}
