package my.company.formatter.handlers;

import my.company.formatter.counter.Counter;
import my.company.formatter.formatter.Handler;

public class ChangeCounterHandler implements Handler {

    private Counter counter;
    private long delta;

    public ChangeCounterHandler(Counter counter, long delta) {
        this.counter = counter;
        this.delta = delta;
    }

    @Override
    public String execute(String input) {
        counter.setCount( counter.getCount() + delta);
        return "";
    }
}
