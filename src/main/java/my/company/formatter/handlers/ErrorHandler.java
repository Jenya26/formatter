package my.company.formatter.handlers;

import my.company.formatter.formatter.Handler;

public class ErrorHandler extends RuntimeException implements Handler {

    public ErrorHandler() {
        super();
    }

    public ErrorHandler( String message) {
        super( message);
    }

    public ErrorHandler( String message, Throwable cause) {
        super( message, cause);
    }

    public ErrorHandler( Throwable cause) {
        super( cause);
    }

    @Override
    public String execute(String input) {
        throw this;
    }
}
