package my.company.formatter.handlers;

import my.company.formatter.counter.Counter;
import my.company.formatter.formatter.Handler;

public class ForeachHandler implements Handler {

    private Counter counter;
    private Handler handler;
    private StringBuilder stringBuilder;

    public ForeachHandler(Handler handler, Counter counter) {
        this.handler = handler;
        this.counter = counter;
        this.stringBuilder = new StringBuilder();
    }

    @Override
    public String execute(String input) {
        stringBuilder.delete( 0, stringBuilder.length());
        long length = counter.getCount();
        for( long i=0; i<length; ++i) {
            stringBuilder.append( handler.execute( input));
        }
        return stringBuilder.toString();
    }

}
