package my.company.formatter.handlers;

import my.company.formatter.formatter.Handler;

public class StringHandler implements Handler {

    private final String string;

    public StringHandler(String string) {
        this.string = string;
    }

    @Override
    public String execute(String input) {
        return this.string;
    }
}
