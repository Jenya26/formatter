package my.company.formatter.handlers;

import my.company.formatter.formatter.Handler;

public class TokenHandler implements Handler {

    private StringBuilder stringBuilder;
    private Handler prefix;
    private Handler body;
    private Handler suffix;

    public TokenHandler( Handler prefix, Handler body, Handler suffix) {
        this.stringBuilder = new StringBuilder();
        this.prefix = prefix;
        this.body = body;
        this.suffix = suffix;
    }

    @Override
    public String execute(String input) {
        stringBuilder.delete( 0, stringBuilder.length());
        return stringBuilder.append( prefix.execute( input)).append( body.execute( input)).append( suffix.execute( input)).toString();
    }

}
