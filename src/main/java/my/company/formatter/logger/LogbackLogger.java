package my.company.formatter.logger;

import my.company.formatter.Logger;

public class LogbackLogger implements Logger{

    private final org.slf4j.Logger logger;

    public LogbackLogger(org.slf4j.Logger logger) {
        this.logger = logger;
    }

    @Override
    public void debug(String string, Object... object) {
        this.logger.debug( string, object);
    }

    @Override
    public void info(String string, Object... object) {
        this.logger.info( string, object);
    }

    @Override
    public void error( String string, Object... object) {
        this.logger.error( string, object);
    }

    @Override
    public void warn(String string, Object... object) {
        this.logger.warn( string, object);
    }
}
