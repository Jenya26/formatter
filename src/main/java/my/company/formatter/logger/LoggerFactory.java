package my.company.formatter.logger;

import my.company.formatter.Logger;

public class LoggerFactory{

    public static <T> Logger getLogger(Class<T> classOfT) {
        return new LogbackLogger( org.slf4j.LoggerFactory.getLogger( classOfT));
    }
}
