package my.company.formatter.streams;

import java.io.ByteArrayOutputStream;

public class ByteOutputStream extends ByteArrayOutputStream implements OutputStream, Closeable {

    @Override
    public void write(String output) {
        super.write( output.getBytes(), 0, output.length());
    }

}
