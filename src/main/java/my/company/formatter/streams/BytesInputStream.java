package my.company.formatter.streams;

import java.io.ByteArrayInputStream;

public class BytesInputStream extends ByteArrayInputStream implements InputStream, Closeable {

    public BytesInputStream(byte[] buf) {
        super(buf);
    }

    @Override
    public char next() {
        return (char) super.read();
    }

    @Override
    public boolean hasNext() {
        return pos < count;
    }

}
