package my.company.formatter.streams;

import java.io.FileNotFoundException;

public class FileInputStream extends java.io.FileInputStream implements InputStream, Closeable {

    public FileInputStream(String name) throws FileNotFoundException {
        super(name);
    }

    @Override
    public char next() throws IOException {
        try {
            return (char) super.read();
        } catch (java.io.IOException e) {
            throw new IOException( "IO error", e);
        }
    }

    @Override
    public boolean hasNext() throws IOException {
        try {
            return available() != 0;
        } catch (java.io.IOException e) {
            throw new IOException( "IO error", e);
        }
    }

    @Override
    public synchronized void reset() {
        try {
            super.reset();
        } catch (java.io.IOException e) {
            throw new IOException( e);
        }
    }
}
