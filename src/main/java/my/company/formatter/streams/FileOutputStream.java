package my.company.formatter.streams;

import java.io.FileNotFoundException;

public class FileOutputStream extends java.io.FileOutputStream implements OutputStream, Closeable {

    public FileOutputStream(String name) throws FileNotFoundException {
        super(name);
    }

    @Override
    public void write(String output) throws IOException {
        try {
            super.write( output.getBytes());
        } catch (java.io.IOException e) {
            throw new IOException( "Write error", e);
        }
    }

}
