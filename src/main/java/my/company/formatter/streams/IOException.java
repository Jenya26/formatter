package my.company.formatter.streams;

public class IOException extends RuntimeException {

    public IOException(){
        super();
    }

    public IOException(String message){
        super( message);
    }

    public IOException(String message, Throwable cause){
        super( message, cause);
    }

    public IOException(Throwable cause){
        super( cause);
    }
}
