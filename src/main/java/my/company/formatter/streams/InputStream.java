package my.company.formatter.streams;

public interface InputStream {

    char next() throws IOException;
    boolean hasNext() throws IOException;

}
