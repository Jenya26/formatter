package my.company.formatter.streams;

public interface OutputStream {

    void write( String output) throws IOException;

}
