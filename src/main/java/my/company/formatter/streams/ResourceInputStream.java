package my.company.formatter.streams;

public class ResourceInputStream implements InputStream, Closeable {

    java.io.InputStream inputStream;

    public ResourceInputStream( String name){
        inputStream = this.getClass().getClassLoader().getResourceAsStream( name);
    }

    @Override
    public char next() throws IOException {
        try {
            return (char) inputStream.read();
        } catch (java.io.IOException e) {
            throw new IOException( e);
        }
    }

    @Override
    public boolean hasNext() throws IOException {
        try {
            return inputStream.available() != 0;
        } catch (java.io.IOException e) {
            throw new IOException( e);
        }
    }

    @Override
    public void close() throws Exception {
        inputStream.close();
    }

}
