package my.company.formatter.tokenizer;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

class Node {

    private boolean isToken;
    private final Map<Character, Node> lists;
    private Node defaultNode;

    Node(boolean isToken, Node defaultNode) {
        this.isToken = isToken;
        this.defaultNode = defaultNode;
        this.lists = new HashMap<>();
    }

    Node(boolean isToken) {
        this( isToken, null);
    }

    Node(Node defaultNode) {
        this( false, defaultNode);
    }

    Node() {
        this( false, null);
    }

    public void addList( Character character, Node node){
        lists.put( character, node);
    }

    public boolean isToken(){
        return isToken;
    }

    public Node getList( Character character) {
        return lists.getOrDefault( character, defaultNode);
    }

    public boolean contains( Character character){
        return lists.containsKey( character);
    }

    public void setDefaultNode( Node node){
        this.defaultNode = node;
    }

    public void setIsToken( boolean isToken){
        this.isToken = isToken;
    }

    public boolean isEmpty() {
        return lists.isEmpty() && defaultNode == null;
    }
}
