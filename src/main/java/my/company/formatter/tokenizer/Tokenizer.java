package my.company.formatter.tokenizer;

import my.company.formatter.streams.InputStream;

public interface Tokenizer {

    String nextToken(InputStream inputStream);

}
