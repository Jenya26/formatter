package my.company.formatter.tokenizer;

import my.company.formatter.streams.InputStream;

public class TreeTokenizer implements Tokenizer {

    private final Node rootNode;
    private Node currentNode;
    private char[] buffer;
    private int size;
    private int pos;
    private int length;

    public TreeTokenizer() {
        this.resize( 1);
        this.rootNode = new Node( new Node());
        this.currentNode = this.rootNode;
        this.pos = 0;
        this.length = 0;
    }

    public void load(InputStream inputStream) {
        StringBuilder token = new StringBuilder();
        char character;
        char separator = ' ';
        while( inputStream.hasNext()) {
            token.delete( 0, token.length());
            while (inputStream.hasNext() && (separator = inputStream.next()) == ' ');
            while (inputStream.hasNext() && (character = inputStream.next()) != separator) {
                token.append(character);
            }
            addToken(token.toString());
            while (inputStream.hasNext() && inputStream.next() == ',');
        }
    }

    @Override
    public String nextToken(InputStream inputStream) {
        pos = length = 0;
        char character;
        currentNode = rootNode;
        while( currentNode != null && !currentNode.isEmpty() && !currentNode.isToken()){
            character = inputStream.next();
            buffer[length++] = character;
            currentNode = currentNode.getList( character);
        }
        return String.valueOf( buffer, pos, length - pos);
    }

    private void resize( int size) {
        char[] temp = new char[size];
        for( int i=0; i<size && i < this.size; ++i){
            temp[i] = buffer[i];
        }
        buffer = temp;
        this.size = size;
    }

    private void addToken(String string) {
        int length = string.length();
        int sign = (length - size) >>> 31;
        resize( sign * size + (1 - sign) * (length--));
        Node currentNode = rootNode;
        for( int i=0; i<length; ++i){
            if( !currentNode.contains( string.charAt( i)))
                currentNode.addList( string.charAt( i), new Node());
            currentNode = currentNode.getList( string.charAt( i));
        }
        currentNode.addList( string.charAt( length), new Node( true));
    }
}
