package my.company.formatter.configuration;

import my.company.formatter.streams.BytesInputStream;
import org.junit.Test;

import static org.junit.Assert.*;

public class MapConfigurationTest {

    @Test
    public void testGetHandler() throws Exception {
        String table = "+-------+-----------------+\n" +
                "| Token |     general     |\n" +
                "+-------+-----------------+\n" +
                "| \"for\" |    \"handler\"    |\n" +
                "+-------+-----------------+";
        BytesInputStream bis = new BytesInputStream( table.getBytes());
        MapConfiguration configuration = new MapConfiguration();
        configuration.load( bis);

        assertEquals( "handler", configuration.getParameter( "for", "general"));
    }

}