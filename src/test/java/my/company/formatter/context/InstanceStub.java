package my.company.formatter.context;

public class InstanceStub {

    private final int expected;

    public InstanceStub(){
        expected = 10;
    }

    public InstanceStub( int expected){
        this.expected = expected;
    }

    public int getExpected() {
        return expected;
    }
}
