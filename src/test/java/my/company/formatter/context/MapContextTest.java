package my.company.formatter.context;

import my.company.formatter.streams.BytesInputStream;
import my.company.formatter.streams.InputStream;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class MapContextTest {

    @Test
    public void testInstance() throws IOException {
        String config = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<!DOCTYPE context [\n" +
                "        <!ELEMENT context (bind)*>\n" +
                "        <!ELEMENT bind (name, path)>\n" +
                "        <!ELEMENT name (#PCDATA)>\n" +
                "        <!ELEMENT path (#PCDATA)>\n" +
                "        ]>\n" +
                "<context>\n" +
                "    <bind>\n" +
                "        <name>InstanceStub</name>\n" +
                "        <path>my.company.formatter.context.InstanceStub</path>\n" +
                "    </bind>\n" +
                "</context>";
        InputStream inputStream = new BytesInputStream( config.getBytes());
        MapContext context = new MapContext();
        FacadeInputStream f = new FacadeInputStream( inputStream);
        context.load( inputStream);
        assertEquals( 10, context.<InstanceStub>getInstance( "InstanceStub").getExpected());
    }
}