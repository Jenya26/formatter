package my.company.formatter.streams;

import org.junit.Test;

import static org.junit.Assert.*;

public class ByteOutputStreamTest {

    @Test
    public void write() throws Exception {
        ByteOutputStream bos = new ByteOutputStream();
        String expected = "asd";
        bos.write( expected);
        assertEquals( expected, bos.toString());
    }
}