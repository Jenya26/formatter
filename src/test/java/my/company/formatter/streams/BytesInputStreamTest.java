package my.company.formatter.streams;

import org.junit.Test;

import static org.junit.Assert.*;

public class BytesInputStreamTest {

    @Test
    public void testNext(){
        String expected = "asd";
        BytesInputStream bis = new BytesInputStream( expected.getBytes());
        for( int i=0; i<expected.length(); ++i) {
            assertEquals(expected.charAt( i), bis.next());
        }
    }

    @Test
    public void testHasNext(){
        String expected = "asd";
        BytesInputStream bis = new BytesInputStream( expected.getBytes());
        for( int i=0; i<expected.length(); ++i) {
            assertTrue( bis.hasNext());
            assertEquals(expected.charAt( i), bis.next());
        }
        assertFalse( bis.hasNext());
    }
}