package my.company.formatter.tokenizer;

import my.company.formatter.streams.BytesInputStream;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TreeTokenizerTest {

    @Test
    public void testNextToken(){
        String config = "\"for\", \"foreach\"";
        BytesInputStream bis = new BytesInputStream( config.getBytes());
        TreeTokenizer tokenizer = new TreeTokenizer();
        tokenizer.load( bis);

        String expected = "wfor   ";
        bis = new BytesInputStream( expected.getBytes());
        assertEquals( "w", tokenizer.nextToken( bis));
        assertEquals( "for", tokenizer.nextToken( bis));
        assertEquals( "   ", tokenizer.nextToken( bis));
    }
}